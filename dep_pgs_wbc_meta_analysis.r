library(meta)

meta.function <- function(dat=dat, analysis.name=analysis.name){
    meta.output <- metagen(Beta,
        SE,
        data=dat,
        studlab=paste(Cohort),
        comb.fixed = TRUE,
        comb.random = FALSE,
        prediction=TRUE,
        sm="SMD")
    #capture.output(meta.output, file=paste0(out.name,"_meta_analysis.txt"))
    Cohort <- "Meta-analysis"
    Analysis <- paste0(analysis.name)
    P.value <- paste0(meta.output$pval.fixed)
    Beta <- paste0(meta.output$TE.fixed)
    SE <- paste0(meta.output$seTE.fixed)
    Lower.CI <- paste0(meta.output$lower.fixed)
    Upper.CI <- paste0(meta.output$upper.fixed)
    numbers <- data.frame(Cohort, Analysis, P.value, Beta, SE, Lower.CI, Upper.CI)
    return(numbers)
}



## original analysis
dat <- read.csv("depression_pgs_wbc_original_analysis_phs_update_07302020.csv", header=T) 

original <- subset(dat, Analysis=="Depression PGS")
dep_cov <- subset(dat, Analysis=="Depression PGS + Depression Diagnosis")
dep_anx_cov <- subset(dat, Analysis=="Depression PGS + Depression Diagnosis + Anxiety Diagnosis")

original.meta <- meta.function(dat=original, analysis.name="Depression PGS")
dep.cov.meta <- meta.function(dat=dep_cov, analysis.name="Depression PGS + Depression Diagnosis")
dep.anx.meta <- meta.function(dat=dep_anx_cov, analysis.name="Depression PGS + Depression Diagnosis + Anxiety Diagnosis")
meta.results <- rbind(original.meta, dep.cov.meta, dep.anx.meta)

all <- rbind(dat, meta.results)
write.csv(all, "depression_pgs_wbc_original_analysis_phs_update_07302020.csv", col.names=T, row.names=F, quote=F)

## Plot
library(ggplot2)
library(stringr)

meta <- all

meta$Order <- ifelse(meta$Cohort=="Meta-analysis", "1",
    ifelse(meta$Cohort=="MGB", "3",
        ifelse(meta$Cohort=="MVP", "4",
            ifelse(meta$Cohort=="VUMC","2",
                ifelse(meta$Cohort=="MSSM","5","0")))))

#META - 1
#MSSM 5
#MVP 4
#PHS 3
#VUMC 2

meta$Analysis2 <- str_wrap(meta$Analysis, width=25)
meta$Analysis2 <- factor(meta$Analysis2, levels=unique(rev(meta$Analysis2)))
meta <- meta[order(meta$Order),]
meta2 <- subset(meta, Cohort=="VUMC" | Cohort=="MGB" | Cohort=="MVP" | Cohort=="MSSM" | Cohort=="Meta-analysis")
meta2$Cohort <- factor(meta2$Cohort, levels=unique(meta2$Cohort))

meta2$Beta <- as.numeric(meta2$Beta)
meta2$Lower.CI <- as.numeric(meta2$Lower.CI)
meta2$Upper.CI <- as.numeric(meta2$Upper.CI)

pdf("depression_pgs_wbc_original_analysis_10022020.pdf", width=10)
p <- ggplot(data=meta2, aes(x=Analysis2, y=Beta, ymin=Lower.CI, ymax=Upper.CI, fill=Cohort, colour=Cohort)) + 
    geom_pointrange(position=position_dodge(width=0.5), size=1) + 
    geom_hline(yintercept=0, lty=2) + 
    coord_flip() + 
    ggtitle(" ") +
    theme_classic() +
    xlab(" ") +
    theme(axis.text.x=element_text(size=16), axis.title.x=element_text(size=14)) +
    theme(legend.title = element_text(size = 12), legend.text = element_text(size=12), axis.text.y=element_text(size=16)) +
    ylab("Depression PGS Beta") +
    scale_colour_manual(values = c("MVP" = "blue3", "VUMC"="goldenrod2", "MGB"="deepskyblue3","MSSM"="purple3","Meta-analysis"="black")) +
    guides(colour=guide_legend(reverse=T), fill=F)
print(p)
dev.off()





## Pathway Meta

dat <- read.csv("direction_meta_07302020_phs_update.csv")
colnames(dat)[2] <- "Cohort"

depression_pgs_to_wbc <- subset(dat, Analysis=="WBC ~ Depression PGS")
wbc_pgs_to_mdd <- subset(dat, Analysis=="Depression Diagnosis ~ WBC PGS")
cond_depression_pgs_to_wbc <- dat[9:12,]


depression_pgs_to_wbc.meta <- meta.function(dat=depression_pgs_to_wbc, analysis.name="WBC ~ Depression PGS")
wbc_pgs_to_mdd.meta <- meta.function(dat=wbc_pgs_to_mdd, analysis.name="Depression Diagnosis ~ WBC PGS")
cond_depression_pgs_to_wbc.meta <- meta.function(dat=cond_depression_pgs_to_wbc, analysis.name="WBC ~ Depression PGS|WBC ")

dat$Pathway <- NULL
colnames(dat)[1] <- "Analysis"

meta <- rbind(dat, depression_pgs_to_wbc.meta, wbc_pgs_to_mdd.meta, cond_depression_pgs_to_wbc.meta)

write.csv(meta, "direction_meta_07302020_phs_update.csv", row.names=F, quote=F)


meta <- read.csv("direction_meta_07302020_phs_update.csv", header=T)
library(ggplot2)
library(stringr)

meta$Order <- ifelse(meta$Cohort=="Meta-analysis", "1",
    ifelse(meta$Cohort=="MGB", "3",
        ifelse(meta$Cohort=="MVP", "4",
            ifelse(meta$Cohort=="VUMC","2",
                ifelse(meta$Cohort=="MSSM","5","0")))))

meta$Analysis2 <- str_wrap(meta$Analysis, width=25)
meta$Analysis2 <- factor(meta$Analysis2, levels=unique(rev(meta$Analysis2)))

meta$Analysis <- factor(meta$Analysis, levels=unique(rev(meta$Analysis)))
meta <- meta[order(meta$Order),]
meta2 <- subset(meta, Cohort=="VUMC" | Cohort=="MGB" | Cohort=="MVP" | Cohort=="MSSM" | Cohort=="Meta-analysis")
meta2$Cohort <- factor(meta2$Cohort, levels=unique(meta2$Cohort))
meta2$Beta <- as.numeric(meta2$Beta)
meta2$Lower.CI <- as.numeric(meta2$Lower.CI)
meta2$Upper.CI <- as.numeric(meta2$Upper.CI)

pdf("direction_meta_100220200.pdf", width=10)
p <- ggplot(data=meta2, aes(x=Analysis, y=Beta, ymin=Lower.CI, ymax=Upper.CI, fill=Cohort, colour=Cohort)) + 
    geom_pointrange(position=position_dodge(width=0.5), size=1) + 
    geom_hline(yintercept=0, lty=2) + 
    coord_flip() + 
    ggtitle(" ") +
    theme_classic() +
    xlab(" ") +
    theme(axis.text.x=element_text(size=16), axis.title.x=element_text(size=14)) +
    theme(legend.title = element_text(size = 12), legend.text = element_text(size=12), , axis.text.y=element_text(size=16)) +
    ylab("PGS Beta") +
    scale_colour_manual(values = c("MVP" = "blue3", "VUMC"="goldenrod2", "MGB"="deepskyblue3","MSSM"="purple3","Meta-analysis"="black"))  +
    guides(colour=guide_legend(reverse=T), fill=F)
print(p)
dev.off()




## mediation meta

wbc <- read.csv("wbc_mediator_levels_85_90_95.csv", header=T)
wbc$SE <- (wbc$upper.prop.med.ci - wbc$prop.mediated)/1.96
colnames(wbc)[6:8] <- c("Beta","Lower.CI","Upper.CI")

wbc.85 <- subset(wbc, treat_percentile=="0.85")
wbc.90 <- subset(wbc, treat_percentile=="0.9")
wbc.95 <- subset(wbc, treat_percentile=="0.95")

wbc.85.meta <- meta.function(dat=wbc.85, analysis.name="WBC.85")
wbc.90.meta <- meta.function(dat=wbc.90, analysis.name="WBC.90")
wbc.95.meta <- meta.function(dat=wbc.95, analysis.name="WBC.95")

wbc.meta <- rbind(wbc.85.meta, wbc.90.meta, wbc.95.meta)
write.table(wbc.meta, "wbc_meta_11-03-2020.csv", row.names=F, quote=F)


mdd <- read.csv("depression_dx_mediator_levels_85_90_95.csv", header=T)
mdd$SE <- (mdd$upper.prop.med.ci - mdd$prop.mediated)/1.96
colnames(mdd)[6:8] <- c("Beta","Lower.CI","Upper.CI")

mdd.85 <- subset(mdd, treat_percentile=="0.85")
mdd.90 <- subset(mdd, treat_percentile=="0.9")
mdd.95 <- subset(mdd, treat_percentile=="0.95")

mdd.85.meta <- meta.function(dat=mdd.85, analysis.name="MDD.85")
mdd.90.meta <- meta.function(dat=mdd.90, analysis.name="MDD.90")
mdd.95.meta <- meta.function(dat=mdd.95, analysis.name="MDD.95")

mdd.meta <- rbind(mdd.85.meta, mdd.90.meta, mdd.95.meta)
write.table(mdd.meta, "mdd_meta_11-03-2020.csv", row.names=F, quote=F)

## make charts 

wbc$seed <- NULL
wbc$sims <- NULL
wbc$treatment <- NULL
wbc$P.value <- NA

wbc.meta$mediator <- "wbc"
colnames(wbc.meta)[1:2] <- c("Cohort", "treat_percentile")
wbc.meta2 <- rbind(wbc, wbc.meta)

wbc.meta2$Prop_mediated <- round(as.numeric(wbc.meta2$Beta), digits=3)
wbc.meta2$SE2 <- round(as.numeric(wbc.meta2$SE), digits=3)
wbc.meta2$Lower.CI2 <- round(as.numeric(wbc.meta2$Lower.CI), digits=3)
wbc.meta2$Upper.CI2 <- round(as.numeric(wbc.meta2$Upper.CI), digits=3)
wbc.meta2$PropMed.SE <- paste0(wbc.meta2$Prop_mediated, " (",wbc.meta2$SE2, ")")
wbc.meta2$CI.95 <- paste0(wbc.meta2$Lower.CI2, " - ", wbc.meta2$Upper.CI2)

write.csv(wbc.meta2, "wbc_meta2.csv")

mdd$seed <- NULL
mdd$sims <- NULL
mdd$treatment <- NULL
mdd$P.value <- NA

mdd.meta$mediator <- "mdd"
colnames(mdd.meta)[1:2] <- c("treat_percentile", "Cohort")
mdd.meta2 <- rbind(mdd, mdd.meta)

mdd.meta2$Prop_mediated <- round(as.numeric(mdd.meta2$Beta), digits=3)
mdd.meta2$SE2 <- round(as.numeric(mdd.meta2$SE), digits=3)
mdd.meta2$Lower.CI2 <- round(as.numeric(mdd.meta2$Lower.CI), digits=3)
mdd.meta2$Upper.CI2 <- round(as.numeric(mdd.meta2$Upper.CI), digits=3)
mdd.meta2$PropMed.SE <- paste0(mdd.meta2$Prop_mediated, " (",mdd.meta2$SE2, ")")
mdd.meta2$CI.95 <- paste0(mdd.meta2$Lower.CI2, " - ", mdd.meta2$Upper.CI2)

write.csv(mdd.meta2, "mdd_meta2.csv")