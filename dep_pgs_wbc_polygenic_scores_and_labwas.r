
## make polygenic scores using PRScs

pheno_name <- "depression"
out_dir <- '/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/'
target_dat <- '/data/davis_lab/sealockj/projects/scripts/mega_geno_data/20200330_biallelic_mega_recalled.chr1-22.grid.EU.filt1_IBD_filt_0.2'
script_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/"
email <- "julia.m.sealock@vanderbilt.edu"
accre_account <- "davis_lab"
PRSice <- "FALSE"
PRSCS <- "TRUE"
prs_cs_base_dat <- "/data/davis_lab/sealockj/cad_mdd_sumstats/howard_depression/PGC_UKB_depression_genome-wide_prscs_base_format.txt"
n_gwas <- '500199'
prs_cs_ld_ref_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/PRScs/ld_files/eur_ld"
LDPred <- "FALSE"
source("/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/prs_comparison.R")

pheno_name <- "depression_on_wbc"
out_dir <- '/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_on_wbc_pgs/'
target_dat <- '/data/davis_lab/sealockj/projects/scripts/mega_geno_data/20200330_biallelic_mega_recalled.chr1-22.grid.EU.filt1_IBD_filt_0.2'
script_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/"
email <- "julia.m.sealock@vanderbilt.edu"
accre_account <- "davis_lab"
PRSice <- "FALSE"
PRSCS <- "TRUE"
prs_cs_base_dat <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/current/depression_wbc_mtcojo/prs_cs/depression_on_wbc_mtcojo.mtcojo.adj_sumstats_prscs_format.txt"
n_gwas <- '500199'
prs_cs_ld_ref_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/PRScs/ld_files/eur_ld"
LDPred <- "FALSE"
source("/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/prs_comparison.R")

pheno_name <- "wbc"
out_dir <- '/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/wbc_pgs/'
target_dat <- '/data/davis_lab/sealockj/projects/scripts/mega_geno_data/20200330_biallelic_mega_recalled.chr1-22.grid.EU.filt1_IBD_filt_0.2'
script_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/"
email <- "julia.m.sealock@vanderbilt.edu"
accre_account <- "davis_lab"
PRSice <- "FALSE"
PRSCS <- "TRUE"
prs_cs_base_dat <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/current/depression_wbc_mtcojo/wbc_sumstats_prscs_format.txt"
n_gwas <- '173480'
prs_cs_ld_ref_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/PRScs/ld_files/eur_ld"
LDPred <- "FALSE"
source("/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/prs_comparison.R")


## cov file

load("/data/davis_lab/sealockj/projects/scripts/sex_strat_prs_phewas/mega_geno_set_phecode_table_02062020.Rdata")
cov <- read.table("/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt", header=T, sep="\t")
grep("296.2", colnames(phecode))
grep("300.1", colnames(phecode))
dep.anx.phe <- phecode[c(1,486,492)]
colnames(dep.anx.phe) <- c("GRID","dep_296.2","anx_300.1")
cov2 <- merge(cov, dep.anx.phe, by="GRID")
write.table(cov2, "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_cov.txt", col.names=T, row.names=F, sep="\t", quote=F)

## depression PGS Labwas's

prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")
covar <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

## dep phecode cov 
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_dep_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","dep_296.2")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_cov.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

## dep/anx phecode cov 
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_dep_anx_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","dep_296.2","anx_300.1")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_cov.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")







prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/direction/depression_phecode.txt"
pheno.name <- "depression_phecode"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_all_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")
covar <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")


## adjustment disorder

## adj rxn phecode cov 
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_adj_rxn_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","adj_rxn_304")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

#adj rxn + dep + anx
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_dep_anx_adj_rxn_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","dep_296.2","anx_300.1","adj_rxn_304")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

## covary for bmi
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_median_bmi_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","medianBMI")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")


## covary for bmi + adj rxn + dep + anx
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_median_bmi_adj_rxn_dep_anx_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","medianBMI","dep_296.2","anx_300.1","adj_rxn_304")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")




#############################
### African Ancestry #######
############################


pheno_name <- "depression"
out_dir <- '/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/'
target_dat <- '/data/davis_lab/actkinsk/labwas/20200511_biallelic_mega_recalled.chr1-22.grid.AA.filt1.maf.005.IBD.filtered.PI_HAT.0.2'
script_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/"
email <- "julia.m.sealock@vanderbilt.edu"
accre_account <- "davis_lab"
PRSice <- "FALSE"
PRSCS <- "TRUE"
prs_cs_base_dat <- "/data/davis_lab/sealockj/cad_mdd_sumstats/howard_depression/PGC_UKB_depression_genome-wide_prscs_base_format.txt"
n_gwas <- '500199'
prs_cs_ld_ref_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/PRScs/ld_files/eur_ld"
LDPred <- "FALSE"
source("/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/prs_comparison.R")


prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_african_ancestry"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")
covar <- "/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200511_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_AA.filt1.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")


load("/data/davis_lab/sealockj/projects/scripts/sex_strat_prs_phewas/mega_geno_set_phecode_table_02062020.Rdata")
cov <- read.table("/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200511_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_AA.filt1.txt", header=T, sep="\t")
grep("296.2", colnames(phecode))
grep("300.1", colnames(phecode))
dep.anx.phe <- phecode[c(1,486,492)]
colnames(dep.anx.phe) <- c("GRID","dep_296.2","anx_300.1")
cov2 <- merge(cov, dep.anx.phe, by="GRID")
write.table(cov2, "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/prs_cs/labwas/20200511_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_AA.filt1_dep_anx_cov.txt", col.names=T, row.names=F, sep="\t", quote=F)


prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_african_ancestry_dep_anx_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","dep_296.2","anx_300.1")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/african_ancestry/prs_cs/labwas/20200511_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_AA.filt1_dep_anx_cov.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")





### control EUR for TUD and smoking
## TUD = 318
## smoking = /data/davis_lab/shared/phenotype_data/biovu/delivered_data/sd_wide_pull/20190514_pull/vitals-smoking-sd-complete-2019-05-14.csv.zip

load("/data/davis_lab/sealockj/projects/scripts/sex_strat_prs_phewas/mega_geno_set_phecode_table_02062020.Rdata")
cov <- read.table("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi.txt", header=T, sep="\t")
grep("318", colnames(phecode))

tud.phe <- phecode[c(1,532)]
colnames(tud.phe) <- c("GRID","tud_318")
cov2 <- merge(cov, tud.phe, by="GRID")
write.table(cov2, "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi_tud.txt", col.names=T, row.names=F, sep="\t", quote=F)

## control for tud
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_tud"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","tud_318")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi_tud.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

## tud and BMI
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_tud_median_bmi"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","tud_318","medianBMI")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi_tud.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")

## tud, BMI, mdd, anx, adj rxn
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
pheno.name <- "depression_pgs_median_bmi_adj_rxn_dep_anx_tud_cov"
working.dir <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/"
labs <- "/data/davis_lab/sealockj/projects/scripts/lab_gwas/sd_wide/indirect_int_h2/20200413_InverseNormal_Medians_ageAdjusted_indirect_int_heritable_labs.RDS"
min_sample_size=100
lab_categories <- "/data/davis_lab/sealockj/projects/scripts/labwas/to_share/shortnames-longnames-categories.csv"
covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","medianBMI","dep_296.2","anx_300.1","adj_rxn_304","tud_318")
covar <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_dep_anx_adj_rxn_cov_median_bmi_tud.txt"
source("/data/davis_lab/sealockj/projects/scripts/labwas/to_share/runLabWAS_v2.R")



## facet wrap plot
setwd("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/")
basic <- read.csv("depression_pgs_labwas.csv", header=T)
dep <- read.csv("depression_pgs_dep_cov_labwas.csv", header=T)
dep_anx <- read.csv("depression_pgs_dep_anx_cov_labwas.csv", header=T)
dep_anx_adj_rxn <- read.csv("depression_pgs_dep_anx_adj_rxn_cov_labwas.csv", header=T)
dep_anx_adj_rxn_bmi <- read.csv("depression_pgs_median_bmi_adj_rxn_dep_anx_cov_labwas.csv", header=T)

tud <- read.csv("depression_pgs_tud_labwas.csv", header=T)
tud_bmi <- read.csv("depression_pgs_tud_median_bmi_labwas.csv", header=T)
dep_anx_adj_rxn_bmi_tud <- read.csv("depression_pgs_median_bmi_adj_rxn_dep_anx_tud_cov_labwas.csv", header=T)

basic$cov <- "basic"
dep$cov <- "Depression"
dep_anx$cov <- "Depression & Anxiety"
dep_anx_adj_rxn$cov <- "Depression & Anxiety & Adj Rxn"
dep_anx_adj_rxn_bmi$cov <- "Depression & Anxiety & Adj Rxn & BMI"
tud$cov <- "TUD"
tud_bmi$cov <- "TUD & BMI"
dep_anx_adj_rxn_bmi_tud$cov <- "Depression & Anxiety & Adj Rxn & BMI & TUD"

results <- rbind(basic, dep, dep_anx, dep_anx_adj_rxn, dep_anx_adj_rxn_bmi, tud, tud_bmi, dep_anx_adj_rxn_bmi_tud)


results$direction <- sign(results$effect_estimate)
results2=results[!is.na(results$pvalue),]
results2$graph_pvalue <- -log10(results2$pvalue)
bonferroni <- .05/nrow(basic)
results2$cov <- factor(results$cov, levels=unique(results2$cov))

pdf("all_sensitivity_analyses_plots_2021-04-23.pdf", height=10.625, width=13.75)
p <- ggplot(data=results2, aes(x=group, y=graph_pvalue)) +
        geom_jitter(aes(size=.2,shape=factor(direction), color=factor(group), fill=factor(group))) +
        geom_text_repel(data=subset(results2, pvalue<bonferroni), aes(label=lab)) +
        scale_shape_manual(values=c(25,24)) +
        geom_hline(yintercept=(-log10(bonferroni)), color="red") +
        geom_hline(yintercept=(-log10(.05)), color="blue") +
        guides(fill=FALSE) +
        guides(size=FALSE) +
        guides(shape=FALSE, colour=FALSE) +
        theme_classic() +
        theme(axis.text.x=element_text(angle=45, hjust=1, size=12), axis.text.y=element_text(size=12), axis.title.y=element_text(size=12), axis.title.x=element_text(size=12), strip.text.x=element_text(size=12)) +
        ylab("-log10(pvalue)") +
        facet_wrap(~ cov, ncol=4)
print(p)
dev.off()


## Wbc forest plot
wbc <- subset(results, lab=="WBC")
wbc$Lower.CI <- wbc$effect_estimate - (1.96*wbc$std_error)
wbc$Upper.CI <- wbc$effect_estimate + (1.96*wbc$std_error)

wbc$cov <- factor(wbc$cov, levels=rev(unique(wbc$cov)))

pdf("all_sensitivity_analyses_wbc_2021-04-23.pdf", width=10)
p <- ggplot(data=wbc, aes(x=cov, y=effect_estimate, ymin=Lower.CI, ymax=Upper.CI, fill=cov, colour=cov)) + 
    geom_pointrange(position=position_dodge(width=0.5), size=1) + 
    geom_hline(yintercept=0, lty=2) + 
    coord_flip() + 
    ggtitle(" ") +
    theme_classic() +
    xlab(" ") +
    theme(axis.text.x=element_text(size=16), axis.title.x=element_text(size=14)) +
    theme(legend.title = element_text(size = 12), legend.text = element_text(size=12), axis.text.y=element_text(size=16)) +
    ylab("Depression PGS Beta") +
    xlab("Covariates") + 
    guides(colour=F, fill=F)
print(p)
dev.off()


### plots for Fig 1

setwd("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/")
basic <- read.csv("depression_pgs_labwas.csv", header=T)
dep_anx_adj_rxn_bmi_tud <- read.csv("depression_pgs_median_bmi_adj_rxn_dep_anx_tud_cov_labwas.csv", header=T)

graph <- function(results=results, name=name){
    results$direction <- sign(results$effect_estimate)
    results2=results[!is.na(results$pvalue),]
    results2$graph_pvalue <- -log10(results2$pvalue)
    bonferroni <- .05/nrow(basic)
    results2$cov <- factor(results$cov, levels=unique(results2$cov))

    pdf(paste0("figure_1_",name,"_2021-04-28.pdf"))
    p <- ggplot(data=results2, aes(x=group, y=graph_pvalue)) +
            geom_jitter(aes(size=.2,shape=factor(direction), color=factor(group), fill=factor(group))) +
            geom_text_repel(data=subset(results2, pvalue<bonferroni), aes(label=lab)) +
            scale_shape_manual(values=c(25,24)) +
            geom_hline(yintercept=(-log10(bonferroni)), color="red") +
            geom_hline(yintercept=(-log10(.05)), color="blue") +
            guides(fill=FALSE) +
            guides(size=FALSE) +
            guides(shape=FALSE, colour=FALSE) +
            theme_classic() +
            ylab("-log10(pvalue)") +
            theme(axis.text.x=element_text(angle=45, hjust=1, size=14), axis.text.y=element_text(size=14), axis.title.y=element_text(size=14), axis.title.x=element_text(size=14), strip.text.x=element_text(size=14)) +
            ylim(c(0,18))
    print(p)
    dev.off()
}

graph(results=basic, name="basic")
graph(results=dep_anx_adj_rxn_bmi_tud, name="dep_anx_adj_rxn_bmi_tud")

## make efigure

setwd("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/labwas/")
basic <- read.csv("depression_pgs_labwas.csv", header=T)
dep <- read.csv("depression_pgs_dep_cov_labwas.csv", header=T)
dep_anx <- read.csv("depression_pgs_dep_anx_cov_labwas.csv", header=T)
dep_anx_adj_rxn <- read.csv("depression_pgs_dep_anx_adj_rxn_cov_labwas.csv", header=T)
dep_anx_adj_rxn_bmi <- read.csv("depression_pgs_median_bmi_adj_rxn_dep_anx_cov_labwas.csv", header=T)
dep_anx_adj_rxn_bmi_tud <- read.csv("depression_pgs_median_bmi_adj_rxn_dep_anx_tud_cov_labwas.csv", header=T)
dep_anx_adj_rxn_bmi_ever_never_smoking <- read.csv("smoking/depression_pgs_ever_smoked_median_bmi_mdd_anx_adj_rxn_labwas.csv", header=T)

basic$cov <- "a."
dep$cov <- "b."
dep_anx$cov <- "c."
dep_anx_adj_rxn$cov <- "d."
dep_anx_adj_rxn_bmi$cov <- "e."
dep_anx_adj_rxn_bmi_tud$cov <- "f."
dep_anx_adj_rxn_bmi_ever_never_smoking$cov <- "g."

results <- rbind(basic, dep, dep_anx, dep_anx_adj_rxn, dep_anx_adj_rxn_bmi, dep_anx_adj_rxn_bmi_tud, dep_anx_adj_rxn_bmi_ever_never_smoking)


results$direction <- sign(results$effect_estimate)
results2=results[!is.na(results$pvalue),]
results2$graph_pvalue <- -log10(results2$pvalue)
bonferroni <- .05/nrow(basic)
results2$cov <- factor(results$cov, levels=unique(results2$cov))

pdf("efigure_sensitivity_analyses_plots_2021-04-28.pdf", height=8.5, width=11)
p <- ggplot(data=results2, aes(x=group, y=graph_pvalue)) +
        geom_jitter(aes(size=.2,shape=factor(direction), color=factor(group), fill=factor(group))) +
        geom_text_repel(data=subset(results2, pvalue<bonferroni), aes(label=lab)) +
        scale_shape_manual(values=c(25,24)) +
        geom_hline(yintercept=(-log10(bonferroni)), color="red") +
        geom_hline(yintercept=(-log10(.05)), color="blue") +
        guides(fill=FALSE) +
        guides(size=FALSE) +
        guides(shape=FALSE, colour=FALSE) +
        theme_classic() +
        theme(axis.text.x=element_text(angle=45, hjust=1, size=14), axis.text.y=element_text(size=14), axis.title.y=element_text(size=14), axis.title.x=element_text(size=14), strip.text.x=element_text(size=14, angle=0, hjust=0, face="bold")) +
        ylab("-log10(pvalue)") +
        facet_wrap(~ cov, ncol=3)
print(p)
dev.off()


