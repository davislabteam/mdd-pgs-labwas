#########################
####### GSMR ############
#########################

/data/davis_lab/singhk/Projects/GWAS_Summarystats/WBC_full.txt
/data/davis_lab/singhk/Projects/GWAS_Summarystats/Depression_full.txt

#SNP A1  A2  freq    b   se  p   N
#Columns are SNP, the effect allele, the other allele, frequency of the effect allele, effect size, standard error, p-value and sample size.

/data/davis_lab/sealockj/projects/scripts/lab_gwas/gcta_1.92.4beta/gcta64 --bfile /data/davis_lab/sealockj/projects/scripts/mega_geno_data/20200330_biallelic_mega_recalled.chr1-22.grid.EU.filt1_IBD_filt_0.2 --gsmr-file mdd_file.txt wbc_file.txt --gsmr-direction 2 --out /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/mdd_wbc_gsmr_result_02042021 --effect-plot

## add heidi outlier method
/data/davis_lab/sealockj/projects/scripts/lab_gwas/gcta_1.92.4beta/gcta64 --bfile /data/davis_lab/sealockj/projects/scripts/mega_geno_data/20200330_biallelic_mega_recalled.chr1-22.grid.EU.filt1_IBD_filt_0.2 --gsmr-file mdd_file.txt wbc_file.txt --gsmr-direction 2 --out /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/mdd_wbc_gsmr_result_heidi_outlier --effect-plot --gsmr2-beta

