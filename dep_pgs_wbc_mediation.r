## Mediation
### edit on 3/10/2020 to make sure that all glm models use a binomial family with a probit link

## R package
library(mediation)


## Specify files 
### IDs in 1st column, variable in 2nd column 
dep_pgs.path <- "/path/to/dep|wbc/pgs/prscs/output"
wbc.path <- "/path/to/age_adj/inv_normalized/wbc/measurement"
depression_phecode.path <- "/path/to/dep/phecode"

### IDs in 1st column, PCs and sex in columns #2-11 (or however many columns you have)
covariates.path <- "/path/to/file/with/pcs/and/sex"


## Read in Data
dep_pgs <- read.table(dep_pgs.path, header=T, sep="")
wbc <- read.table(wbc.path, header=T, sep="\t")
depression_phecode <- read.table(depression_phecode.path, header=T, sep="")
covariates <- read.table(covariates.path, header=T, sep="")

colnames(dep_pgs)[1:2] <- c("ID","depression_pgs")
colnames(wbc)[1:2] <- c("ID","wbc")
colnames(depression_phecode)[1:2] <- c("ID","depression_phecode")
colnames(covariates)[1] <- "ID"

## Merge files together 

dat <- Reduce(function(x,y) merge(x,y,by="ID"), list(dep_pgs, wbc, depression_phecode, covariates))

## subset to people with complete data....the mediation package doesn't like missing data
dat.complete <- dat[complete.cases(dat),]

## scale polygenic score
dat.complete$depression_pgs <- as.numeric(scale(dat.complete$depression_pgs))


## Scheme 1
### WBC mediates the association between depression pgs and depression diagnosis
wbc_predict_depression_phecode <- glm(depression_phecode ~ wbc + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))

depression_pgs_predict_phecode <- glm(depression_phecode ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))

depression_pgs_predict_wbc <- lm(wbc ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)

depression_pgs_and_wbc_predict_phecode <- glm(depression_phecode ~ depression_pgs + wbc + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))

## get stats
wbc_predict_depression_phecode_stats <- coef(summary(wbc_predict_depression_phecode))[2,]
depression_pgs_predict_phecode_stats <- coef(summary(depression_pgs_predict_phecode))[2,]
depression_pgs_predict_wbc_stats <- coef(summary(depression_pgs_predict_wbc))[2,]
depression_pgs_and_wbc_predict_phecode_stats <- coef(summary(depression_pgs_and_wbc_predict_phecode))[2,]

wbc.med.out.stats <- rbind(wbc_predict_depression_phecode_stats, depression_pgs_predict_phecode_stats, depression_pgs_predict_wbc_stats, depression_pgs_and_wbc_predict_phecode_stats)
write.table(wbc.med.out.stats, "wbc_mediation_scheme_regression_stats.txt", col.names=T, row.names=T, sep="\t", quote=F)


## define covariate list
cov.list <- list("sex","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")

## run mediation
## we're going to run 1k bootstrap runs 5x using 5 different seeds. if the p-values remain stable across different seeds then we can stop here. if they're not stable, then continue on using 5k bootstraps and then 10k if still not stable.
## this function and commands below it will run the analysis at different seeds and then write the output to a file. 

## if you're unsure what constitutes a stable p-value, feel free to ask me!

## 1k boostraps 
bootstrap1 <- function(seed=seed, sims=sims){
    set.seed(seed)
    wbc.med.out <- mediate(depression_pgs_predict_wbc, depression_pgs_and_wbc_predict_phecode, treat="depression_pgs", mediator="wbc", covariates=cov.list, boot=TRUE, sims=sims)
    wbc.med.out.sum <- summary(wbc.med.out)
    capture.output(wbc.med.out.sum, file=paste0("depression_mediator_",sims,"_bootstrap_seed_",seed,".txt"))
}


## 1k boostraps 

bootstrap1(seed=55, sims=1000)
bootstrap1(seed=98, sims=1000)
bootstrap1(seed=550, sims=1000)
bootstrap1(seed=293, sims=1000)
bootstrap1(seed=375, sims=1000)


## if not stable, try 5K bootstraps

bootstrap1(seed=55, sims=5000)
bootstrap1(seed=98, sims=5000)
bootstrap1(seed=550, sims=5000)
bootstrap1(seed=293, sims=5000)
bootstrap1(seed=375, sims=5000)


## if still not stable, try 10K bootstraps

bootstrap1(seed=55, sims=10000)
bootstrap1(seed=98, sims=10000)
bootstrap1(seed=550, sims=10000)
bootstrap1(seed=293, sims=10000)
bootstrap1(seed=375, sims=10000)






## Scheme 2
## depression diagnosis mediates relationship between depression pgs and wbc

## make sure depression phecode is 0/1 and numeric or else mediate will give an error

dat.complete$depression_phecode <- as.numeric(dat.complete$depression_phecode)

depression_phecode_predict_wbc <- lm(wbc ~ depression_phecode + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)

depression_pgs_predict_wbc <- lm(wbc ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)

depression_pgs_predict_phecode <- glm(depression_phecode ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))

depression_pgs_and_phecode_predict_wbc <- lm(wbc ~ depression_pgs + depression_phecode + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)



## get regression stats
depression_phecode_predict_wbc_stats <- coef(summary(depression_phecode_predict_wbc))[2,]
depression_pgs_predict_wbc_stats <- coef(summary(depression_pgs_predict_wbc))[2,]
depression_pgs_predict_phecode_stats <- coef(summary(depression_pgs_predict_phecode))[2,]
depression_pgs_and_phecode_predict_wbc_stats <- coef(summary(depression_pgs_and_phecode_predict_wbc))[2,]

wbc.med.out.stats <- rbind(depression_phecode_predict_wbc_stats, depression_pgs_predict_wbc_stats, depression_pgs_predict_phecode_stats, depression_pgs_and_phecode_predict_wbc_stats)
write.table(wbc.med.out.stats, "depression_diagnosis_mediation_scheme_regression_stats.txt", col.names=T, row.names=T, sep="\t", quote=F)



## run mediation: same bootstrapping method as before

bootstrap2 <- function(seed=seed, sims=sims){
    set.seed(seed)
    diagnosis.med.out <- mediate(depression_pgs_predict_phecode, depression_pgs_and_phecode_predict_wbc, treat="depression_pgs", mediator="depression_phecode", covariates=cov.list, boot=TRUE, sims=sims)
    diagnosis.med.out.sum <- summary(diagnosis.med.out)
    capture.output(diagnosis.med.out.sum, file=paste0("depression_diagnosis_mediator_",sims,"_bootstrap_seed_",seed,".txt"))
}

## 1k boostraps 

bootstrap2(seed=55, sims=1000)
bootstrap2(seed=98, sims=1000)
bootstrap2(seed=550, sims=1000)
bootstrap2(seed=293, sims=1000)
bootstrap2(seed=375, sims=1000)


## if not stable, try 5K bootstraps

bootstrap2(seed=55, sims=5000)
bootstrap2(seed=98, sims=5000)
bootstrap2(seed=550, sims=5000)
bootstrap2(seed=293, sims=5000)
bootstrap2(seed=375, sims=5000)


## if still not stable, try 10K bootstraps

bootstrap2(seed=55, sims=10000)
bootstrap2(seed=98, sims=10000)
bootstrap2(seed=550, sims=10000)
bootstrap2(seed=293, sims=10000)
bootstrap2(seed=375, sims=10000)


## at the end, a quite a few files have probably been written, but I just need the "depression_diagnosis_mediation_scheme_regression_stats.txt", "wbc_mediation_scheme_regression_stats.txt", and the results from a stable run of both mediation analyses
