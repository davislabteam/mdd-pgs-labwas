## summary data lists
mdd /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/depression_sumstats.raw
bmi /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/giant_bmi_sumstats_2018.raw

wbc /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/wbc_sumstats.raw
bmi /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/giant_bmi_sumstats_2018.raw


##### MDD |  BMI 

#!/bin/bash
#SBATCH --mail-user=julia.m.sealock@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1 
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=50GB
#SBATCH--job-name="MDD|BMI"
#SBATCH --output=/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/mdd/mdd_on_bmi_slurm.out

module purge

/data/davis_lab/sealockj/projects/scripts/mtcojo/gcta_1.91.4beta/gcta64 \
--mbfile /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mtcojo/ref_data.txt \
--mtcojo-file /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/mdd/summary_data.list \
--ref-ld-chr /data/davis_lab/sealockj/projects/scripts/mtcojo/eur_w_ld_chr/ \
--w-ld-chr /data/davis_lab/sealockj/projects/scripts/mtcojo/eur_w_ld_chr/ \
--out /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/mdd/mdd_on_bmi_result


#### WBC | BMI

#!/bin/bash
#SBATCH --mail-user=julia.m.sealock@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1 
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=50GB
#SBATCH--job-name="WBC|BMI"
#SBATCH --output=/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/wbc/mdd_on_bmi_slurm.out

module purge

/data/davis_lab/sealockj/projects/scripts/mtcojo/gcta_1.91.4beta/gcta64 \
--mbfile /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mtcojo/ref_data.txt \
--mtcojo-file /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/wbc/summary_data.list \
--ref-ld-chr /data/davis_lab/sealockj/projects/scripts/mtcojo/eur_w_ld_chr/ \
--w-ld-chr /data/davis_lab/sealockj/projects/scripts/mtcojo/eur_w_ld_chr/ \
--out /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/wbc/wbc_on_bmi_result




######### GSMR
awk '{print $1,$2,$3,$4,$9,$10,$11,$8}' mdd_on_bmi_result.mtcojo.cma > mdd_on_bmi_result.mtcojo.cma.output.txt

awk '{print $1,$2,$3,$4,$9,$10,$11,$8}' wbc_on_bmi_result.mtcojo.cma > wbc_on_bmi_result.mtcojo.cma.output.txt

mdd_on_bmi /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/mdd/mdd_on_bmi_result.mtcojo.cma.output.txt

wbc_on_bmi /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/wbc/wbc_on_bmi_result.mtcojo.cma.output.txt


## add heidi outlier method
/data/davis_lab/sealockj/projects/scripts/lab_gwas/gcta_1.92.4beta/gcta64 --bfile /data/davis_lab/sealockj/projects/scripts/mega_geno_data/20200330_biallelic_mega_recalled.chr1-22.grid.EU.filt1_IBD_filt_0.2 \
--gsmr-file /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/mdd_on_bmi_file.txt /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/wbc_on_bmi_file.txt \
--gsmr-direction 2 \
--out /data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/bmi_mtcojo/mdd_wbc_cond_on_bmi_gsmr_result \
--effect-plot  \
--gsmr2-beta


### plot

source("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/gsmr_plot.r")
gsmr_data <- read_gsmr_data("mdd_wbc_cond_on_bmi_gsmr_result.eff_plot.gz")
gsmr_summary(gsmr_data)

pdf("mdd_exposure_wbc_outcome_bmi_cond.pdf")
plot_gsmr_effect(gsmr_data, "mdd_on_bmi", "wbc_on_bmi", colors()[75])
dev.off()

pdf("wbc_exposure_mdd_outcome_bmi_cond.pdf")
plot_gsmr_effect(gsmr_data, "wbc_on_bmi", "mdd_on_bmi", colors()[75])
dev.off()



plot_snp_effect2 = function(expo_str, outcome_str, bxy, bzx, bzx_se, bzy, bzy_se, effect_col=colors()[75]) {
    vals = c(bzx-bzx_se, bzx+bzx_se)
    xmin = min(vals); xmax = max(vals)
    vals = c(bzy-bzy_se, bzy+bzy_se)
    ymin = min(vals); ymax = max(vals)
    plot(bzx, bzy, pch=20, cex=0.8, bty="n", cex.axis=1.1, cex.lab=1.2,
         col=effect_col, ylim=c(-.06, .06), xlim=c(-.2, .2),
         #col=effect_col, xlim=c(xmin, xmax), ylim=c(ymin, ymax),
         #ylab="WBC | BMI",
         #xlab="MDD | BMI")
         xlab="WBC",
         ylab="MDD")
    if(!is.na(bxy)) abline(0, bxy, lwd=1.5, lty=2, col="dim grey")
    ## Standard errors
    nsnps = length(bzx)
    for( i in 1:nsnps ) {
        # x axis
        xstart = bzx[i] - bzx_se[i]; xend = bzx[i] + bzx_se[i]
        ystart = bzy[i]; yend = bzy[i]
        segments(xstart, ystart, xend, yend, lwd=1.5, col=effect_col)
        # y axis
        xstart = bzx[i]; xend = bzx[i]
        ystart = bzy[i] - bzy_se[i]; yend = bzy[i] + bzy_se[i]
        segments(xstart, ystart, xend, yend, lwd=1.5, col=effect_col)
    }
}


plot_gsmr_effect2 = function(gsmr_data, expo_str, outcome_str, effect_col=colors()[75]) {
    resbuf = gsmr_snp_effect(gsmr_data, expo_str, outcome_str);
    bxy = resbuf$bxy
    bzx = resbuf$bzx; bzx_se = resbuf$bzx_se;
    bzy = resbuf$bzy; bzy_se = resbuf$bzy_se;
    # plot
    plot_snp_effect2(expo_str, outcome_str, bxy, bzx, bzx_se, bzy, bzy_se, effect_col)
}

pdf("wbc_exposure_mdd_outcome_bmi_cond2.pdf")
plot_gsmr_effect2(gsmr_data, "wbc_on_bmi", "mdd_on_bmi", colors()[75])
dev.off()


pdf("mdd_exposure_wbc_outcome_bmi_cond2.pdf")
plot_gsmr_effect2(gsmr_data, "mdd_on_bmi", "wbc_on_bmi", colors()[75])
dev.off()



source("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mendelian_randomization/gsmr/gsmr_plot.r")
gsmr_data <- read_gsmr_data("../mdd_wbc_gsmr_result_heidi_outlier.eff_plot.gz")
gsmr_summary(gsmr_data)

## redo orig plots
pdf("mdd_exposure_wbc_outcome_orig.pdf")
plot_gsmr_effect2(gsmr_data, "mdd", "wbc", colors()[75])
dev.off()


pdf("wbc_exposure_mdd_outcome_orig.pdf")
plot_gsmr_effect2(gsmr_data, "wbc", "mdd", colors()[75])
dev.off()

