## Julia Sealock
## 09/24/19


## Age adjustment and inverse normalization of lab values

# d = dataframe with median lab value ("median") and age at median ("age_median") for each individual
# step 1 of script will add a column "resid" which is the age adjusted residual of the lab value
# step 2 will add another column "in_pheno" which is the inverse normalization of the age adjusted value

library(rms) ## last version had typo "rcs"
library(RNOmni) ## I use version 0.7.1 but older versions work, too

d$resid <- as.numeric(lm(median ~ rcs(age_median, 3), data=d)$resid)
d$in_pheno <- rankNorm(d$resid) ## older version of RNOmni uses "rankNormalize" instead of "rankNorm"


## linear model for regression
### dat is a dataframe with median age adjusted inverse normalized WBC, depression PGS, PCs1-10, and sex

dat$PGS_scaled <- scale(dat$PGS) # z-score scale PGS
output <- lm(WBC ~ PGS_scaled + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10 + SEX, data=dat)
original_stats <- as.data.frame(coef(summary(output))[2,]) ## get results for PGS
colnames(original_stats) <- "original"
original_ci <- as.data.frame(confint.lm(output)[2,])
colnames(original_ci) <- "original"
original <- rbind(original_stats, original_ci)

## extract depression and anxiety phecodes (296.2 & 300.1) and add to dataframe, dat
## repeat with depression phecode (296.2) as covariate 
output <- lm(WBC ~ PGS_scaled + DEPRESSION_PHECODE + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10 + SEX, data=dat)
depression_cov_stats <- as.data.frame(coef(summary(output))[2,])
colnames(depression_cov_stats) <- "depression_cov"
depression_cov_ci <- as.data.frame(confint.lm(output)[2,])
colnames(depression_cov_ci) <- "depression_cov"
depression_cov <- rbind(depression_cov_stats, depression_cov_ci)

## and with depression (296.2) and anxiety phecodes (300.1) as covariate 
output <- lm(WBC ~ PGS_scaled + DEPRESSION_PHECODE + ANXIETY_PHECODE + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10 + SEX, data=dat)
depression_anx_cov_stats <- as.data.frame(coef(summary(output))[2,])
colnames(depression_anx_cov_stats) <- "depression_anx_cov"
depression_anx_cov_ci <- as.data.frame(confint.lm(output)[2,])
colnames(depression_anx_cov_ci) <- "depression_anx_cov"
depression_anx_cov <- rbind(depression_anx_cov_stats, depression_anx_cov_ci)

results <- data.frame(original, depression_cov, depression_anx_cov)