## confounder analysis
## 1. phewas depression PGS and WBC
## 2. find overlapping phenos
## 3. bin into categories
## 4. control for groups

## phewas

pheno.name <- "depression_pgs"
prs <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt"
medical.home <- "FALSE"
working.directory <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/confounder_analysis/dep_pgs_phewas/"
covariate_file <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/confounder_analysis/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_median_age.txt"
phewas.covariates.to.use <- "GENDER,PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,median_age_of_record"
sex.strat <- "FALSE"
source("/data/davis_lab/sealockj/projects/scripts/sex_strat_prs_phewas/phewas.R")

pheno.name <- "wbc"
prs <- "/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20190715_sdwide_pull/selected_lab_summaries_04142020/WBC/20200609_WBC_InverseNormal_Medians_ageAdjusted.txt"
medical.home <- "FALSE"
working.directory <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/confounder_analysis/wbc_phewas/"
covariate_file <- "/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/confounder_analysis/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_median_age.txt"
phewas.covariates.to.use <- "GENDER,PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,median_age_of_record"
sex.strat <- "FALSE"
source("/data/davis_lab/sealockj/projects/scripts/sex_strat_prs_phewas/phewas.R")

## find overlap

wbc <- read.table("wbc_phewas/wbc_all.txt", header=T, sep="\t")
dep <- read.table("dep_pgs_phewas/depression_pgs_all.txt", header=T, sep="\t")

wbc.bonf <- subset(wbc, bonferroni=="TRUE")
dep.bonf <- subset(dep, bonferroni=="TRUE")

wbc.bonf2 <- wbc.bonf[c(1,18,19,7,5)]
dep.bonf2 <- dep.bonf[c(1,18,19,7,5)]

colnames(wbc.bonf2)[c(4:5)] <- c("wbc_pvalue","wbc_beta")
colnames(dep.bonf2)[c(4:5)] <- c("dep_pvalue","dep_beta")

nrow(wbc.bonf)
#469
nrow(dep.bonf)
#66

common <- merge(wbc.bonf2, dep.bonf2, by="description")
nrow(common)
#32
write.table(common, "common_bonferroni_phenotypes_wbc_and_mdd_pgs_06152020.txt", col.names=T, row.names=F, sep="\t", quote=F)

## FDR 
wbc.fdr <- subset(wbc, fdr=="TRUE")
dep.fdr <- subset(dep, fdr=="TRUE")

wbc.fdr2 <- wbc.fdr[c(1,18,19,7,5)]
dep.fdr2 <- dep.fdr[c(1,18,19,7,5)]

colnames(wbc.fdr2)[c(4:5)] <- c("wbc_pvalue","wbc_beta")
colnames(dep.fdr2)[c(4:5)] <- c("dep_pvalue","dep_beta")

common <- merge(wbc.fdr2, dep.fdr2, by="description")

write.table(common, "common_fdr_phenotypes_wbc_and_mdd_pgs_06152020.txt", col.names=T, row.names=F, sep="\t", quote=F)


## 


load("/data/davis_lab/sealockj/projects/scripts/sex_strat_prs_phewas/mega_geno_set_phecode_table_02062020.Rdata")


cardiovascular <- c("411.3","433","428.1","411.4","401.1","401","411","411.2","418","411.1")
liver <- c("070","070.3")
obesity <- c("250","278.11","278.1","278","250.2")
celiac <- c("557.1")
pain <- c("338.1","338","798")
psychiatric <- c("292.4","313.1","296.1","313","300.9","318")
respiratory<- c("327.3","495","496","496.2","512.7")

match(cardiovascular, names(phecode))
# [1] 769 843 830 770 760 759 766 768 782 767 #10
match(liver, names(phecode))
#[1] 27 30 #2
match(obesity, names(phecode))
#[1] 249 388 387 386 256 #5
match(celiac, names(phecode))
#[1] 1100
match(pain, names(phecode))
#[1] 570 569 1722 #2
match(psychiatric, names(phecode))
#[1] 475 520 485 519 500 532 #6
match(respiratory, names(phecode))
#[1] 542 948 952 954 983 #5

cardiovascular <- phecode[c(1,769, 843, 830, 770, 760, 759, 766, 768, 782, 767)] 
liver <- phecode[c(1,27, 30)] 
obesity <- phecode[c(1,249, 388, 387, 386, 256)] 
celiac <- phecode[c(1,1100)] 
pain <- phecode[c(1,570, 569, 1722)] 
psychiatric <- phecode[c(1,475, 520, 485, 519, 500, 532)] 
respiratory <- phecode[c(1,542, 948, 952, 954, 983)] 

library(dplyr)
case.controls <- function(dat=dat, pheno=pheno){
    cases <- filter_all(dat, any_vars(.=="TRUE"))
    cases$status <- "TRUE"
    controls <- filter_at(dat, vars(-GRID),all_vars(.=="FALSE"))  
    controls$status <- "FALSE"
    all <- rbind(cases, controls)
    grid.status <- all[c("GRID","status")]
    grid.status$status <- as.logical(grid.status$status)
    colnames(grid.status)[2] <- paste0(pheno)
    return(grid.status)
    }

cardiovascular.status <- case.controls(dat=cardiovascular, pheno="cardiovascular")
liver.status <- case.controls(dat=liver, pheno="liver")
obesity.status <- case.controls(dat=obesity, pheno="obesity")
celiac.status <- case.controls(dat=celiac, pheno="celiac")
pain.status <- case.controls(dat=pain, pheno="pain")
psychiatric.status <- case.controls(dat=psychiatric, pheno="psychiatric")
respiratory.status <- case.controls(dat=respiratory, pheno="respiratory")

phenos <- Reduce(function(x,y) merge(x,y, by="GRID", all=TRUE), list(cardiovascular.status, liver.status, obesity.status, celiac.status, pain.status, psychiatric.status, respiratory.status))

## regressions

cov <- read.table("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/confounder_analysis/20200330_MEGA.GRID.RACE.ETH.GEN.batch.PCs.covariates_EU.filt1_median_age.txt", header=T, sep="\t")
wbc <- read.table("/data/davis_lab/shared/bioVU_labs_for_use/quality_labs/biovu_20190715_sdwide_pull/selected_lab_summaries_04142020/WBC/20200609_WBC_InverseNormal_Medians_ageAdjusted.txt", header=T, sep="")
colnames(wbc) <- c("GRID","WBC")
prs <- read.table("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/depression_pgs/prs_cs/depression_prs_cs_results.profile_to_use.txt",header=T, sep="")
colnames(prs) <- c("GRID","PRS")
prs$PRS <- scale(prs$PRS)

dat <- Reduce(function(x,y) merge(x,y, by="GRID"), list(phenos, cov, wbc, prs))

write.table(dat, "depression_pgs_wbc_grouped_common_phenos_06222020.txt", col.names=T, row.names=F, sep="\t", quote=F)

covariates <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10","median_age_of_record")

regression <- function(var2) {
    regression <- lm(as.formula(paste("WBC~PRS+var2+",paste(covariates, collapse="+"))), data=dat)
    coef <- coef(summary(regression))
    stats <- (coef[2,])
    return(stats)
    }

    rg <- lapply(dat[2:8],regression)

    rg_df <- as.data.frame(rg)
    rg_df_t <- as.data.frame(t(rg_df))
    rg_df_t2 <- rg_df_t %>% rownames_to_column("group")
    rg_df_t3 <- rg_df_t2[c(1,2,3,5)]


all.cov <- lm(WBC~PRS+GENDER+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10+median_age_of_record+cardiovascular+liver+obesity+celiac+pain+psychiatric+respiratory, data=dat)
    all.cov.sum <- coef(summary(all.cov))
    all.cov.sum2 <- all.cov.sum[2,]
    all.cov.sum2_df <- as.data.frame(all.cov.sum2)
    all.cov.sum2_df_t <- as.data.frame(t(all.cov.sum2_df))
    all.cov.sum2_df_t2 <- all.cov.sum2_df_t %>% rownames_to_column("group")
    all.cov.sum2_df_t3 <- all.cov.sum2_df_t2[c(1,2,3,5)]
    all.cov.sum2_df_t3$group <- "all_covs"


none <- lm(WBC~PRS+GENDER+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10+median_age_of_record, data=dat)
    none.sum <- coef(summary(none))
    none.sum2 <- none.sum[2,]
    none.sum2_df <- as.data.frame(none.sum2)
    none.sum2_df_t <- as.data.frame(t(none.sum2_df))
    none.sum2_df_t2 <- none.sum2_df_t %>% rownames_to_column("group")
    none.sum2_df_t3 <- none.sum2_df_t2[c(1,2,3,5)]
    none.sum2_df_t3$group <- "basic"

results <- rbind(none.sum2_df_t3, all.cov.sum2_df_t3, rg_df_t3)
colnames(results)[3] <- "SE"
results$Lower_CI <- results$Estimate - (1.96*results$SE)
results$Upper_CI <- results$Estimate + (1.96*results$SE)

write.table(results, "depression_pgs_wbc_confounder_group_regression_results_06222020.txt", col.names=T, row.names=F, sep="\t", quote=F)

## figures


#all <- read.csv("~/Desktop/F31/f31 figs/confounder_regressions.csv", header=T)
library(ggplot2)

all <- read.table("depression_pgs_wbc_confounder_group_regression_results_06222020-fig.txt", header=T, sep="\t")
all <- all[order(all$Order),]
all$Phenotype <- factor(all$Phenotype, levels=unique(rev(all$Phenotype)))

pdf("confounder_regression_forest_plot_06222020.pdf")
p <- ggplot(data=all, aes(x=Phenotype, y=Estimate, ymin=Lower_CI, ymax=Upper_CI, fill=group, colour=group)) + 
    geom_pointrange(position=position_dodge(width=0.5), size=1) + 
    geom_hline(yintercept=0, lty=2) + 
    coord_flip() + 
    ggtitle(" ") +
    theme_classic() +
    guides(fill=FALSE, colour=FALSE) +
    xlab(" ") +
    theme(axis.text=element_text(size=14), axis.title.x=element_text(size=12)) +
    ylab("Depression PGS Beta") +
    scale_colour_manual(values = c("basic" = "black", 
        "cardiovascular" = "red3", 
        "psychiatric" = "darkorange2",
        "obesity" = "lightgoldenrod2", 
        "respiratory" = "chartreuse3", 
        "hepatic" = "turquoise3",
        "pain" = "dodgerblue2",
        "celiac" = "purple"))
print(p)
dev.off()



dat <- read.table("common_bonferroni_phenotypes_wbc_and_mdd_pgs_06222020-2.txt", header=T, sep="\t")
dat$Group <- factor(dat$Group2, levels=unique(dat$Group2))
pdf("pie_chart-2.pdf")
pie <- ggplot(dat, aes(x = "", fill = Group)) + 
  geom_bar(width = 1) +
  theme(axis.line = element_blank(), 
        plot.title = element_text(hjust=0.5)) + 
  labs(fill="group", 
       x="pheno", 
       y=NULL) + 
    coord_polar(theta = "y", start=0) +
    theme_void() +
    theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid  = element_blank()) +
    scale_fill_manual(name="Group",
        values = c("Cardiovascular" = "red3", 
        "Psychiatric" = "darkorange2",
        "Obesity" = "lightgoldenrod2", 
        "Respiratory" = "chartreuse3", 
        "Hepatic" = "turquoise3",
        "Pain" = "dodgerblue2",
        "Autoimmune" = "purple"))
print(pie)
dev.off()




pdf("median_untransformed_wbc_by_depression_pgs_decile.pdf")
ggplot(dat, aes(x=PGS_decile, y=WBC, group=PGS_decile2)) + 
    geom_boxplot() + 
    geom_hline(yintercept=4, color="blue") + geom_hline(yintercept=11, color="blue") + 
    theme_classic() + 
    scale_x_continuous(breaks=seq(1,10,1)) +
    xlab("Depression PGS Decile") +
    theme(axis.text.x=element_text(size=16), axis.title.x=element_text(size=14)) +
    theme(axis.text.y=element_text(size=16), axis.title.y=element_text(size=14)) +
    ylab("WBC (thou cells/uL)")
dev.off()



#### create one pheno for alll phenos

dat <- read.table("depression_pgs_wbc_grouped_common_phenos_06222020.txt", header=T, sep="\t")
cases <- subset(dat, cardiovascular=="TRUE" | liver=="TRUE" | obesity=="TRUE" | celiac=="TRUE" | pain=="TRUE" | psychiatric=="TRUE" | respiratory=="TRUE")
controls <- subset(dat, cardiovascular=="FALSE" & liver=="FALSE" & obesity=="FALSE" & celiac=="FALSE" & pain=="FALSE" & psychiatric=="FALSE" & respiratory=="FALSE")

cases$all_phenos <- "TRUE"
controls$all_phenos <- "FALSE"

dat2 <- rbind(cases, controls)

all.cov <- lm(WBC~PRS+GENDER+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10+median_age_of_record+all_phenos, data=dat2)
    all.cov.sum <- coef(summary(all.cov))
    all.cov.sum2 <- all.cov.sum[2,]
    all.cov.sum2_df <- as.data.frame(all.cov.sum2)
    all.cov.sum2_df_t <- as.data.frame(t(all.cov.sum2_df))
    all.cov.sum2_df_t2 <- all.cov.sum2_df_t %>% rownames_to_column("group")
    all.cov.sum2_df_t3 <- all.cov.sum2_df_t2[c(1,2,3,5)]
    all.cov.sum2_df_t3$group <- "all_covs_one_pheno"

#               group   Estimate  Std. Error     Pr(>|t|)
# all_covs_one_pheno 0.02981587 0.003928333 3.255339e-14

summary(as.factor(dat2$all_phenos))
#FALSE  TRUE 
# 7075 43123



###########################
## phenotype interactions 

#### metabolic disorder 
###### cardiovascular*obesity

metabolic <- lm(WBC~PRS+GENDER+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10+median_age_of_record+cardiovascular+obesity+cardiovascular*obesity, data=dat)
#    Estimate Std. Error t value Pr(>|t|) 
#PRS 0.0308533  0.0040700   7.581 3.50e-14 ***

psych.pain <- lm(WBC~PRS+GENDER+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10+median_age_of_record+psychiatric+pain+psychiatric*pain, data=dat)
#    Estimate Std. Error t value Pr(>|t|)  
#PRS 0.0324617  0.0048959   6.630 3.40e-11 ***



##### New figure with all in 1 pheno and interctions

library(ggplot2)

all <- read.table("depression_pgs_wbc_confounder_group_regression_results_with_interaction_12022020.txt", header=T, sep="\t")
all <- all[order(all$Order),]
all$Phenotype <- factor(all$Phenotype, levels=unique(rev(all$Phenotype)))

pdf("confounder_regression_forest_plot_with_interactions_12012020.pdf")
p <- ggplot(data=all, aes(x=Phenotype, y=Estimate, ymin=Lower_CI, ymax=Upper_CI, fill=group, colour=group)) + 
    geom_pointrange(position=position_dodge(width=0.5), size=1) + 
    geom_hline(yintercept=0, lty=2) + 
    coord_flip() + 
    ggtitle(" ") +
    theme_classic() +
    guides(fill=FALSE, colour=FALSE) +
    xlab(" ") +
    theme(axis.text=element_text(size=14), axis.title.x=element_text(size=12)) +
    ylab("Depression PGS Beta") +
    scale_colour_manual(values = c("basic" = "black", 
        "cardiovascular" = "red3", 
        "psychiatric" = "darkorange2",
        "obesity" = "lightgoldenrod2", 
        "respiratory" = "chartreuse3", 
        "hepatic" = "turquoise3",
        "pain" = "dodgerblue2",
        "celiac" = "purple",
        "metabolic" = "salmon1",
        "psych_pain" = "darkgreen"))
print(p)
dev.off()
