
## run mediation

dat <- read.table("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mediation/prop_mediated_06-05/cbc_diff/mdd_pgs_wbc_cbc_diff_dataset_with_mdd_dx_06242020.txt", header=T, sep="\t")
cov.list <- c("GENDER","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")
library(mediation)

bootstrap <- function(seed=seed, treat=treat, main=main, alt=alt, outcome=outcome, sims=sims, dat=dat){
    set.seed(seed)
    date <- Sys.Date()

    control.percentile <- as.numeric(quantile(dat$depression_pgs, c(.50)))
    treat.percentile <- as.numeric(quantile(dat$depression_pgs, treat_percentile))

    med.out <- multimed(outcome=outcome, med.main=main, med.alt=alt, treat=treat, covariates=cov.list, data=dat, sims=sims, control.value=control.percentile, treat.value=treat.percentile)
    med.out.sum <- summary(med.out)

    capture.output(med.out.sum, file=paste0(treat,"_treat_",outcome,"_outcome_",main,"_main_med_bootstrap",sims,"_seed_",seed,"_",date,"_summary.txt"))

    total = med.out$tau.ci[c(1)]
    total_upper = med.out$tau.ci[c(2)]

    acme = med.out$d.ave.ci[1,1]
    acme_upper = med.out$d.ave.ci[2,1]

    ade = med.out$z.ave.ci[1,1]
    ade_upper = med.out$z.ave.ci[2,1]

    lower = rbind(acme, ade)
    upper = rbind(acme_upper, ade_upper)

    colnames(lower)[1] <- c("2.5%")
    colnames(upper)[1] <- c("97.5%")

    lower2 <- rbind(lower, total)
    upper2 <- rbind(upper, total_upper)
    ci <- cbind(lower2, upper2)

    capture.output(ci, file=paste0(treat,"_treat_",outcome,"_outcome_",main,"_main_med_bootstrap",sims,"_seed_",seed,"_",date,"conf_int.txt"))
   
}



#######################################
## MDD PGS --> WBC diff --> MDD dx  ###
#######################################

### ntabs as main med
setwd("/data/davis_lab/sealockj/projects/mh_labwas/mdd/howard_dep_prs/sdwide_current/mediation/prop_mediated_06-05/cbc_diff/")

bootstrap(seed=550, treat="mdd_pgs", main="ntabs_resid_inv_norm", alt=c("basoabs_resid_inv_norm","monabs_resid_inv_norm","lymabs_resid_inv_norm","eosabs_resid_inv_norm"), outcome="mdd_dx", sims=1000, dat=dat)

## monabs as main med
bootstrap(seed=550, treat="mdd_pgs", main="monabs_resid_inv_norm", alt=c("basoabs_resid_inv_norm","ntabs_resid_inv_norm","lymabs_resid_inv_norm","eosabs_resid_inv_norm"), outcome="mdd_dx", sims=1000, dat=dat)

##lymabs as main med
bootstrap(seed=550, treat="mdd_pgs", main="lymabs_resid_inv_norm", alt=c("basoabs_resid_inv_norm","ntabs_resid_inv_norm","monabs_resid_inv_norm","eosabs_resid_inv_norm"), outcome="mdd_dx", sims=1000, dat=dat)

##basoabs as main med
bootstrap(seed=550, treat="mdd_pgs", main="basoabs_resid_inv_norm", alt=c("lymabs_resid_inv_norm","ntabs_resid_inv_norm","monabs_resid_inv_norm","eosabs_resid_inv_norm"), outcome="mdd_dx", sims=10000, dat=dat)

##eooabs as main med
bootstrap(seed=550, treat="mdd_pgs", main="eosabs_resid_inv_norm", alt=c("lymabs_resid_inv_norm","ntabs_resid_inv_norm","monabs_resid_inv_norm","basoabs_resid_inv_norm"), outcome="mdd_dx", sims=1000, dat=dat)