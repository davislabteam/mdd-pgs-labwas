
## Direction of assoication 

## Specify files 
### IDs in 1st column, variable in 2nd column
wbc_pgs.path <- "/path/to/wbc/pgs/prscs/output" 
dep_on_wbc_pgs.path <- "/path/to/dep|wbc/pgs/prscs/output"
wbc.path <- "/path/to/age_adj/inv_normalized/wbc/measurement"
depression_phecode.path <- "/path/to/dep/phecode"


### IDs in 1st column, PCs and sex in columns #2-11
covariates.path <- "/path/to/file/with/pcs/and/sex"


## Read in Data
wbc_pgs <- read.table(wbc_pgs.path, header=T, sep="")
dep_on_wbc_pgs <- read.table(dep_on_wbc_pgs.path, header=T, sep="")
wbc <- read.table(wbc.path, header=T, sep="\t")
depression_phecode <- read.table(depression_phecode.path, header=T, sep="")
covariates <- read.table(covariates.path, header=T, sep="")

colnames(wbc_pgs)[1:2] <- c("ID","wbc_pgs")
colnames(dep_on_wbc_pgs)[1:2] <- c("ID","depression_on_wbc_pgs")
colnames(wbc)[1:2] <- c("ID","wbc")
colnames(depression_phecode)[1:2] <- c("ID","depression_phecode")
colnames(covariates)[1] <- "ID"

## Merge files together 

dat <- Reduce(function(x,y) merge(x,y,by="ID"), list(wbc_pgs, dep_on_wbc_pgs, wbc, depression_phecode, covariates)) ## might have to change "by" field to your ID column name

## z-score scale polygenic scores

dat$wbc_pgs <- as.numeric(scale(dat$wbc_pgs))
dat$depression_on_wbc_pgs <- as.numeric(scale(dat$depression_on_wbc_pgs))


## Analysis 1
### find association with wbc pgs and depression phecode

dep_phecode_on_wbc_pgs <- glm(depression_phecode ~ wbc_pgs + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat, family=binomial)
dep_phecode_on_wbc_pgs_stats <- coef(summary(dep_phecode_on_wbc_pgs))[2,] ## extract the stats associated with wbc_pgs...if wbc_pgs is right after the tilda this will work, otherwise change the [2,] to the corresponding row in the summary output

## Analysis 2
### find association of depression|WBC PGS with WBC

wbc_on_cond_depression_pgs <- lm(wbc ~ depression_on_wbc_pgs + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat)
wbc_on_cond_depression_pgs_stats <- coef(summary(wbc_on_cond_depression_pgs))[2,]

stats <- rbind(dep_phecode_on_wbc_pgs_stats, wbc_on_cond_depression_pgs_stats)
write.table(stats, "depression_pgs_wbc_direction_association_analysis.txt", col.names=T, row.names=T, sep="\t", quote=F) 



## done - send the written stats file over 


