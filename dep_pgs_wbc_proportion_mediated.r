## Mediation
### edit on 3/10/2020 to make sure that all glm models use a binomial family with a probit link
### edit on 10/02/2020 to dichotimize pgs to get prop mediated
### edit on 06/08/2021 to specify values of mdd pgs in the model, not percentiles 

## R package
library(mediation)


## Specify files 
### IDs in 1st column, variable in 2nd column 
dep_pgs.path <- "/path/to/mdd/pgs"
wbc.path <- "/path/to/int/age-adjusted/wbc"
depression_phecode.path <- "/path/to/mdd/phecode" #296.2

### IDs in 1st column, PCs and sex in columns #2-11 (or however many columns you have)
covariates.path <- "/path/to/covariates"


## Read in Data
dep_pgs <- read.table(dep_pgs.path, header=T, sep="")
wbc <- read.table(wbc.path, header=T, sep="\t")
depression_phecode <- read.table(depression_phecode.path, header=T, sep="")
covariates <- read.table(covariates.path, header=T, sep="")

colnames(dep_pgs)[1:2] <- c("ID","depression_pgs")
colnames(wbc)[1:2] <- c("ID","wbc")
colnames(depression_phecode)[1:2] <- c("ID","depression_phecode")
colnames(covariates)[1] <- "ID"
colnames(covariates)[2] <- "sex"

## Merge files together 

dat <- Reduce(function(x,y) merge(x,y,by="ID"), list(dep_pgs, wbc, depression_phecode, covariates))
dat2 <- dat[c(1:5,11:20)]
## subset to people with complete data....the mediation package doesn't like missing data
dat.complete <- dat2[complete.cases(dat2),]

## scale polygenic score
dat.complete$depression_pgs <- as.numeric(scale(dat.complete$depression_pgs))

## Scheme 1
### WBC mediates the association between depression pgs and depression diagnosis
wbc_predict_depression_phecode <- glm(depression_phecode ~ wbc + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))
depression_pgs_predict_phecode <- glm(depression_phecode ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))
depression_pgs_predict_wbc <- lm(wbc ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)
depression_pgs_and_wbc_predict_phecode <- glm(depression_phecode ~ depression_pgs + wbc + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))


## define covariate list
cov.list <- list("sex","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")

## run mediation
## run mediation at seed = 550 
## bootraps: = 1,000


bootstrap1 <- function(treatment=treatment, mediator=mediator, seed=seed, sims=sims, treat_percentile=treat_percentile){
    set.seed(seed)
    
    control.percentile <- as.numeric(quantile(dat.complete$depression_pgs, c(.50)))
    treat.percentile <- as.numeric(quantile(dat.complete$depression_pgs, treat_percentile))

    wbc.med.out <- mediate(depression_pgs_predict_wbc, depression_pgs_and_wbc_predict_phecode, treat=treatment, mediator=mediator, covariates=cov.list, boot=TRUE, sims=sims, control.value=control.percentile, treat.value=treat.percentile)

    prop.mediated = wbc.med.out$n.avg
    prop.mediated.pvalue = wbc.med.out$n.avg.p
    prop.mediated.ci = wbc.med.out$n.avg.ci
    lower.prop.med.ci = t(data.frame(prop.mediated.ci))[1,1]
    upper.prop.med.ci = t(data.frame(prop.mediated.ci))[1,2]

    stats = data.frame(treatment, mediator, treat_percentile, control.percentile, treat.percentile, seed, sims, prop.mediated, prop.mediated.pvalue, lower.prop.med.ci, upper.prop.med.ci)
    return(stats)

}

wbc_med_treatment_85 <- bootstrap1(treatment="depression_pgs", mediator="wbc", seed=550, sims=1000, treat_percentile=.85)
wbc_med_treatment_90 <- bootstrap1(treatment="depression_pgs", mediator="wbc", seed=550, sims=1000, treat_percentile=.90)
wbc_med_treatment_95 <- bootstrap1(treatment="depression_pgs", mediator="wbc", seed=550, sims=1000, treat_percentile=.95)


## Scheme 2
## depression diagnosis mediates relationship between depression pgs and wbc
## make sure depression phecode is 0/1 and numeric or else mediate will give an error

dat.complete$depression_phecode <- as.numeric(dat.complete$depression_phecode)
depression_phecode_predict_wbc <- lm(wbc ~ depression_phecode + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)
depression_pgs_predict_wbc <- lm(wbc ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)
depression_pgs_predict_phecode <- glm(depression_phecode ~ depression_pgs + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete, family=binomial(link="probit"))
depression_pgs_and_phecode_predict_wbc <- lm(wbc ~ depression_pgs + depression_phecode + sex + PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 + PC9 + PC10, data=dat.complete)


## get regression stats
bootstrap2 <- function(treatment=treatment, mediator=mediator, seed=seed, sims=sims, treat_percentile=treat_percentile){
    set.seed(seed)
    
    control.percentile <- as.numeric(quantile(dat.complete$depression_pgs, c(.50)))
    treat.percentile <- as.numeric(quantile(dat.complete$depression_pgs, treat_percentile))
    
    diagnosis.med.out <- mediate(depression_pgs_predict_phecode, depression_pgs_and_phecode_predict_wbc, treat=treatment, mediator=mediator, covariates=cov.list, boot=TRUE, sims=sims, control.value=control.percentile, treat.value=treat.percentile)

    prop.mediated = diagnosis.med.out$n.avg
    prop.mediated.pvalue = diagnosis.med.out$n.avg.p
    prop.mediated.ci = diagnosis.med.out$n.avg.ci
    lower.prop.med.ci = t(data.frame(prop.mediated.ci))[1,1]
    upper.prop.med.ci = t(data.frame(prop.mediated.ci))[1,2]

    stats = data.frame(treatment, mediator, treat_percentile, control.percentile, treat.percentile, seed, sims, prop.mediated, prop.mediated.pvalue, lower.prop.med.ci, upper.prop.med.ci)
    return(stats)
}

dx_med_treatment_85 <- bootstrap2(treatment="depression_pgs", mediator="depression_phecode", seed=550, sims=1000, treat_percentile=.85)
dx_med_treatment_90 <- bootstrap2(treatment="depression_pgs", mediator="depression_phecode", seed=550, sims=1000, treat_percentile=.90)
dx_med_treatment_95 <- bootstrap2(treatment="depression_pgs", mediator="depression_phecode", seed=550, sims=1000, treat_percentile=.95)

### save results 
all <- rbind(wbc_med_treatment_85, wbc_med_treatment_90, wbc_med_treatment_95, dx_med_treatment_85, dx_med_treatment_90, dx_med_treatment_95)
write.table(all, "mdd_pgs_wbc_mediator_analyses_treatment_levels_85_90_95.txt", col.names=T, row.names=F, quote=F,sep="\t")

